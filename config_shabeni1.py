params = {'data_path': './RosData/Shabeni1/',
          'plotname': 'shabeni1',
          'ignition_time':  '2014-08-22_13:00:00',
          'diag_resolution_facor': 2,
          'flag_restart': True, #if False data are loaded from shp else from previous stored npy file
          'ros_max_plot': 5,
          }
params_navashni={
    'fuelLoad': 4777., #kg/ha
    'moisture': 25., #%
    'temperatue': 32, #C
    'humidity': 23,   #%
    'windSpeed': 0.45 #m/s
}
