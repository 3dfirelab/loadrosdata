# README #


### What is this repository for? ###

* this repository is made to upload ROS shape file from the KNP14 data set
* it plots map and pdf at a specified resolution
* inputs for the 4 fires are in config file.
* original resolution is 1m, it can be lower using the parameter `diag_resolution_facor` in the config file

### How do I get set up? ###

* to run it: `python loadRosData.py -i <fire name>` 
* fire names are: `skukuza4`, `skukuza6`, `shabeni1`, `shabeni3` 
* Requiered python libraries: `gdal, scipy, numpy, matploltib`
* Data need to be uploaded separately, eg:
```
mkdir ./RosData/
cd RosData
mv <Download Dir>/KNP14RosData_20210928_cnnv14_dx1m.tar.gz . 
tar -xvf /KNP14RosData_20210928_cnnv14_dx1m.tar.gz
```
* `./RosData/` is the default data directory. it can be modified in the config files.

### Who do I talk to? ###

* Repo owner: Ronan Paugam
