import socket
import sys, os, argparse
import pdb 
import importlib
import numpy as np 
import datetime 
import shapefile
import matplotlib.pyplot as plt 
import glob
from past.utils import old_div
from osgeo import gdal,osr,ogr
from scipy import stats


#homebrewed 
import tools as map_tools



###################################################
def loadGrid(inputInfo):
    #load domain data 
    #-------------------------------
    print ('load Grid')
    maps_fire = np.load(inputInfo['data_path']+'maps_fire_diag_res.npy')
    maps_fire = maps_fire.view(np.recarray)
   
    ignitionTime = datetime.datetime.strptime(inputInfo['ignition_time'], "%Y-%m-%d_%H:%M:%S")    

    return maps_fire, ignitionTime



###################################################
def ShrinkToRes(inputInfo, maps_fire_raw):

    diag_resolution_facor = inputInfo['diag_resolution_facor']
    if diag_resolution_facor ==1: return maps_fire_raw, maps_fire_raw.shape

    #shrink to diagnostic resolution 
    #-------------------------------
    diag_res_cte_shape = (int(round(old_div(1.*maps_fire_raw.grid_e.shape[0],diag_resolution_facor),0)), int(round(old_div(1.*maps_fire_raw.grid_e.shape[1],diag_resolution_facor),0)))
    maps_fire =  np.zeros(diag_res_cte_shape, dtype=np.dtype([('grid_e',float),('grid_n',float),('plotMask',float),('plotMask_original',float),('mask_burnNoburn',float)]))
    maps_fire = maps_fire.view(np.recarray)
    maps_fire.grid_e = map_tools.downgrade_resolution(maps_fire_raw.grid_e, diag_res_cte_shape, flag_interpolation='min')
    maps_fire.grid_n = map_tools.downgrade_resolution(maps_fire_raw.grid_n, diag_res_cte_shape, flag_interpolation='min')
    
    plotMask_name = 'plotMask' if ('plotMask' in maps_fire_raw.dtype.names)  else 'mask'
    if 'mask_nodata' in list(inputInfo.keys()):
        print('apply mask no data on plotMask')
        mask_burnNoburn = np.load(out_dir_npy+inputInfo['mask_nodata'])
        plotMask_here = np.where(mask_burnNoburn!=1, maps_fire_raw[plotMask_name], np.zeros_like(mask_burnNoburn) )
    else: 
        mask_burnNoburn = np.zeros_like(maps_fire.grid_e)
        plotMask_here = maps_fire_raw[plotMask_name]
   
    maps_fire.plotMask_original = map_tools.downgrade_resolution(maps_fire_raw[plotMask_name], diag_res_cte_shape, flag_interpolation='average')
    maps_fire.plotMask_original = np.where(maps_fire.plotMask_original>1.3, 2, 0)

    maps_fire.plotMask =          map_tools.downgrade_resolution(plotMask_here,                diag_res_cte_shape, flag_interpolation='average')
    maps_fire.plotMask =          np.where( maps_fire.plotMask>1.3                             , 2,  maps_fire.plotMask)
    maps_fire.plotMask =          np.where((maps_fire.plotMask>0.3) & (maps_fire.plotMask<=1.3), 1,  maps_fire.plotMask)
    maps_fire.plotMask =          np.where((maps_fire.plotMask<=0.3)                           , 0,  maps_fire.plotMask)
    
    maps_fire.mask_burnNoburn =   map_tools.downgrade_resolution(mask_burnNoburn,                diag_res_cte_shape, flag_interpolation='average')
    maps_fire.mask_burnNoburn =   np.where( maps_fire.plotMask>0.3, 1, 0) 

    arrivalTime = np.zeros(diag_res_cte_shape) - 999
    arrivalTime_timeSeries = np.zeros(diag_res_cte_shape) - 999
    arrivalTime_edgepixel = np.zeros(diag_res_cte_shape) # to store info if the pixel is close to the edge of the picture. 
                                                         # if so it will be remove from the arrival time after cleaning up 
    #remove road from plotmask if present
    idx = np.where(maps_fire.plotMask==1)
    maps_fire.plotMask[idx] = 0
    #plt.imshow(maps_fire.plotMask.T,origin='lower',interpolation='nearest'); plt.show()

    return maps_fire, diag_res_cte_shape


###################################################
def loadRosShapeFile(inputInfo, maps_fire_raw, newshape, out_dir_Wkdir):

    flag_restart = inputInfo['flag_restart']
    plotname     = inputInfo['plotname']

    print ('load ROS shapeFile')
    #---------------------------
    if not(flag_restart) or (not(os.path.isfile(out_dir_Wkdir+'/{:s}_ros_magnitude.npy'.format(plotname.lower())))):
        ros_direction = np.zeros_like(maps_fire_raw.grid_e)
        ros_magnitude = np.zeros_like(maps_fire_raw.grid_e)
        ctr = shapefile.Reader(out_dir_shp+plotname+"_ros.shp")
        fields_name = np.array([ctr.fields[i][0] for i in range(len(ctr.fields))])
        
        records = ctr.shapeRecords() #will store the geometry separately
        for i, record in enumerate(records):
            pts        = record.shape.points # in UTM 
            attributes = record.record       #will show you the attributes

            idx_ = np.where( (np.abs(maps_fire_raw.grid_e+.5*dx-attributes[0])<dx/2) & (np.abs(maps_fire_raw.grid_n+.5*dy-attributes[1])<dy/2))
            ros_magnitude[idx_] = attributes[2]
            ros_direction[idx_] = attributes[3]
        
        #plt.imshow(ros_magnitude.T, origin='lower'); plt.show()
        #pdb.set_trace()
       
        np.save(out_dir_Wkdir+'/{:s}_ros_magnitude.npy'.format(plotname.lower()),ros_magnitude)
        np.save(out_dir_Wkdir+'/{:s}_ros_direction.npy'.format(plotname.lower()),ros_direction)

    else: 
        ros_magnitude = np.load(out_dir_Wkdir+'/{:s}_ros_magnitude.npy'.format(plotname.lower()))
        ros_direction = np.load(out_dir_Wkdir+'/{:s}_ros_direction.npy'.format(plotname.lower()))

    ros_magnitude = map_tools.downgrade_resolution(ros_magnitude, newshape, flag_interpolation='average')
    ros_direction = map_tools.downgrade_resolution(ros_direction, newshape, flag_interpolation='average')
    
    return ros_magnitude, ros_direction



###################################################
def loadQualityMetric(out_dir_geoTiff, newshape):
    print ('load quality metric')
    #------------------------------
    qualityFileName = glob.glob(out_dir_geoTiff+'*quality*')[0]
    ds = gdal.Open(qualityFileName)
    band = ds.GetRasterBand(1)
    ros_quality = band.ReadAsArray()[::-1].T

    return map_tools.downgrade_resolution(ros_quality, newshape, flag_interpolation='min')



###################################################
if __name__ == '__main__':
###################################################


    importlib.reload(map_tools)

    parser = argparse.ArgumentParser(description='this is the driver of the GeorefCam Algo.')
    parser.add_argument('-i','--input', help='Input run name',required=True)
    args = parser.parse_args()

    inputConfig = importlib.machinery.SourceFileLoader('config_'+args.input,os.getcwd()+'/config_'+args.input+'.py').load_module()
    inputInfo = inputConfig.params
    inputFuel = inputConfig.params_navashni

    #directory
    out_dir     = inputInfo['data_path']
    out_dir_shp = out_dir + 'shapeFile/'
    out_dir_geoTiff = out_dir + 'geotiff/'
    out_dir_wkdir     = out_dir + 'Wkdir/'
    map_tools.ensure_dir(out_dir_wkdir) 

    #load grid
    maps_fire_raw, ignitionTime = loadGrid(inputInfo)
    maps_fire, maps_shape = ShrinkToRes(inputInfo, maps_fire_raw)
    dx = maps_fire.grid_e[1,1]-maps_fire.grid_e[0,0]
    dy = maps_fire.grid_n[1,1]-maps_fire.grid_n[0,0]


    #load data
    ros_magnitude, ros_direction = loadRosShapeFile(inputInfo, maps_fire_raw, maps_shape, out_dir_wkdir )
    ros_quality = loadQualityMetric(out_dir_geoTiff, maps_shape)

    #print fuel load info
    for key in inputFuel.keys():
        if   key == 'fuelLoad': unit = 'kg/ha'
        elif key == 'moisture': unit = '%'
        elif key == 'temperatue': unit = 'C'
        elif key == 'humidity': unit = '%'
        elif key == 'windSpeed': unit = 'm/s'
        else: unit = ''
        print ('{:s} : {:.1f} {:s}'.format(key, inputFuel[key], unit))
    
    #set Figure
    fig = plt.figure(figsize=(12,6))


    #plot ros map
    xi_m = maps_fire.grid_e+.5*dx-maps_fire.grid_e[0,0]
    yi_m = maps_fire.grid_n+.5*dy-maps_fire.grid_n[0,0]
    normal_x = np.cos(np.radians((360-ros_direction))+np.pi/2)
    normal_y = np.sin(np.radians((360-ros_direction))+np.pi/2)
    mask_ = maps_fire.plotMask

    scale_quiver = 70
    
    ax = plt.subplot(121)
    idx_mask = np.where((ros_magnitude>0)&(mask_==2))
    qm = ax.quiver( xi_m[idx_mask], yi_m[idx_mask], normal_x[idx_mask], normal_y[idx_mask], ros_magnitude[idx_mask], scale=scale_quiver, units='width',clim=[0,inputInfo['ros_max_plot']])
    ax.set_aspect(1.)
    ax.set_xlabel('x (m)')
    ax.set_ylabel('y (m)')
    ax.set_title('resolution: dx={:.1f}m'.format(dx))

    #plot ros pdf
    idx = np.where((maps_fire.plotMask ==2) &(ros_magnitude>=0))
    kernel = stats.gaussian_kde(ros_magnitude[idx].flatten())
    power_range = np.linspace(old_div(np.log(max([ros_magnitude[idx].min(),1.e-3])),np.log(10)), old_div(np.log(ros_magnitude[idx].max()),np.log(10)), 100 )
    ros_scale = np.exp( power_range * np.log(10) )
    pdf = kernel(ros_scale)

    ax = plt.subplot(122)
    ax.plot(ros_scale,pdf)
    ax.set_xlabel('PDF')
    ax.set_ylabel('ROS (m/s)')

    plt.show()
