params = {'data_path': './RosData/Skukuza6/',
          'plotname': 'skukuza6',
          'ignition_time':  '2014-08-26_10:59:16',
          'diag_resolution_facor': 2,
          'flag_restart': True, #if False data are loaded from shp else from previous stored npy file
          'ros_max_plot': 5,
          }
params_navashni={
    'fuelLoad': 2654., #kg/ha
    'moisture': 22.8,
    'temperatue': 30, #C
    'humidity': 60,   #%
    'windSpeed': 2.63 #m/s
}
