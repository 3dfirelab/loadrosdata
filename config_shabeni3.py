params = {'data_path': './RosData/Shabeni3/',
          'plotname': 'shabeni3',
          'ignition_time':  '2014-08-22_11:00:35',
          'diag_resolution_facor': 2,
          'flag_restart': True, #if False data are loaded from shp else from previous stored npy file
          'ros_max_plot': 5,
          }

params_navashni={
    'fuelLoad': 4678., #kg/ha
    'moisture': 16.9,
    'temperatue': 28, #C
    'humidity': 42,   #%
    'windSpeed': 2.71 #m/s
    }
