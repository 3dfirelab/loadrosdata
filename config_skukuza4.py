params = {'data_path': './RosData/Skukuza4/',
          'plotname': 'skukuza4_05',
          'ignition_time':  '2014-08-26_13:26:00',
          'diag_resolution_facor': 2,
          'flag_restart': True, #if False data are loaded from shp else from previous stored npy file
          'ros_max_plot': 5,
          }
params_navashni={
    'fuelLoad': 4128., #kg/ha
    'moisture': 15.4,
    'temperatue': 33, #C
    'humidity': 48,   #%
    'windSpeed': 1.42 #m/s
}
